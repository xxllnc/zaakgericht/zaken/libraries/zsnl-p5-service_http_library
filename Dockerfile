FROM registry.gitlab.com/xxllnc/zaakgericht/zaken/libraries/zsnl-p5-platform:b98f31f6

WORKDIR /tmp/zaaksysteem-service-http-src

COPY Makefile.PL .

RUN apt-get update \
    && apt-get install --assume-yes cmake \
    && cpanm --installdeps Log::Log4perl::Appender::Fluent \
    && cpanm --notest Log::Log4perl::Appender::Fluent

RUN cpanm --installdeps . \
 && rm -rf ~/.cpanm \
 || (cat ~/.cpanm/work/*/build.log && false)

COPY . .

RUN perl Makefile.PL && make install
