use ExtUtils::MakeMaker;

WriteMakefile(
    NAME => 'Zaaksysteem::Service::HTTP',
    VERSION => '0.001',
    AUTHOR => 'Rudolf Leermakers <rudolf@mintlab.nl>',
    ABSTRACT => 'Zaaksysteem (micro)service HTTP library',
    TEST_REQUIRES => {
        'Test::Class::Moose' => 0,
        'Test::Compile' => 0,
        'Test::Simple' => 0
    },
    PREREQ_PM => {
        'Moose' => 0,
        'Config::General' => 0,
        'Log::Log4perl' => 0,
        'Log::Log4perl::Appender::Fluent' => 0,
        'Log::Log4perl::Layout::JSON' => 0,
        'Log::Log4perl::MDC' => 0,
        'HTTP::Entity::Parser::JSON' => 0,
        'Net::Statsd' => 0,
        'Plack' => 0,
        'Starman' => 0,
        'JSON::XS' => 0,
        'LockFile::Simple' => 0,
        'Module::Pluggable' => 0,
        'Module::Runtime' => 0
    }
);
